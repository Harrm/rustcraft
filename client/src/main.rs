
use rustcraft::application::{Application, VulkanApplication};

fn main() {
    let app = VulkanApplication::create(None);
    app.run();
}
