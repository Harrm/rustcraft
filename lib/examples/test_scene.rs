use cgmath::{Matrix4, Vector3};
use rustcraft::{
    application::{Application, VulkanApplication},
    gameplay::StaticMeshComponent,
};

fn main() {
    let mut application = VulkanApplication::create(None);

    for x in 0..(1000) {
        for y in 0..1 {
            for z in 0..(1) {
                let transform = Vector3::new(x as f32 * 5f32, y as f32 * 5f32, z as f32 * 5f32);
                let world = &mut application.current_world;
                let component = Box::new(StaticMeshComponent::create(
                    std::path::Path::new("data/cube.obj").to_owned(),
                    std::path::Path::new("data/Grass.png").to_owned(),
                    Matrix4::from_translation(transform),
                    world
                ));
                let obj = world.spawn_object(transform);
                obj.add_component(component);
            }
        }
    }
    
    application.run();
}
