use std::time::Instant;

use vulkano::device::{Device, DeviceExtensions, physical::PhysicalDevice};
use vulkano::instance::{Instance};
use vulkano::Version;
use vulkano_win::VkSurfaceBuild;
use winit::event::{Event, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::WindowBuilder;

use crate::gameplay;
use crate::render::{RenderSubsystem};
use crate::subsystem::{Subsystem, SystemEvent};

pub trait Application {
    fn run(self) -> !;
}

pub struct VulkanApplication {
    event_loop: Option<EventLoop<()>>,
    subsystems: Vec<Box<dyn Subsystem>>,

    pub current_world: gameplay::World,

    last_update: Instant,
}

impl VulkanApplication {
    pub fn create(startup_world: Option<&std::path::Path>) -> VulkanApplication {
        let required_extensions = vulkano_win::required_extensions();

        println!(
            "Available layers: {:?}",
            vulkano::instance::layers_list()
                .unwrap()
                .map(|prop| prop.name().to_owned())
                .collect::<Vec<_>>()
        );
        let mut desired_layers = std::collections::HashSet::<&str>::from_iter(vec![
            "VK_LAYER_KHRONOS_validation",
            "VK_LAYER_LUNARG_standard_validation",
            "VK_LAYER_NV_optimus",
        ]);
        let mut found_layers = Vec::<String>::new();

        for layer in vulkano::instance::layers_list().unwrap() {
            if desired_layers.contains(layer.name()) {
                found_layers.push(layer.name().to_owned());
                desired_layers.remove(layer.name());
            }
        }
        println!("Chosen layers: {:?}", found_layers);

        let instance = Instance::new(
            None,
            Version::V1_2,
            &required_extensions,
            found_layers.iter().map(|l| l.as_str()),
        )
        .unwrap();

        let event_loop = EventLoop::new();
        let surface = WindowBuilder::new()
            .build_vk_surface(&event_loop, instance.clone())
            .unwrap();

        PhysicalDevice::enumerate(&instance).for_each(|device| {
            println!(
                "Found device: {} (type: {:?})",
                device.properties().device_name,
                device.properties().device_type,
            );
        });
        let physical = PhysicalDevice::enumerate(&instance).next().unwrap();
        println!(
            "Using device: {} (type: {:?})",
            physical.properties().device_name,
            physical.properties().device_type,
        );
        let queue_family = physical
            .queue_families()
            .find(|&q| q.supports_graphics() && surface.is_supported(q).unwrap_or(false))
            .unwrap();
        let device_ext = DeviceExtensions {
            khr_swapchain: true,
            khr_portability_subset: true,
            ..DeviceExtensions::none()
        };
        let (device, mut queues) = Device::new(
            physical,
            physical.supported_features(),
            &device_ext,
            [(queue_family, 0.5)].iter().cloned(),
        )
        .unwrap();
        let queue = queues.next().unwrap();

        let render_subsystem = Box::new(RenderSubsystem::create(
            device.clone(),
            queue.clone(),
            surface.clone(),
            surface.capabilities(physical).unwrap(),
        ));

        let current_world = if let Some(_) = startup_world {
            unimplemented!()
        } else {
            gameplay::World {
                objects: vec![],
                render_scene: render_subsystem.get_scene(),
            }
        };

        VulkanApplication {
            event_loop: Some(event_loop),
            subsystems: vec![render_subsystem],
            current_world: current_world,

            last_update: Instant::now(),
        }
    }
}

impl Application for VulkanApplication {
    fn run(mut self) -> ! {
        self.event_loop
            .take()
            .unwrap()
            .run(move |event, _, control_flow| {
                let system_event =  
                match event {
                    Event::WindowEvent {
                        event: WindowEvent::CloseRequested,
                        ..
                    } => {
                        *control_flow = ControlFlow::Exit;
                        Some(SystemEvent::Shutdown)
                    }
                    Event::WindowEvent {
                        event: WindowEvent::Resized(_),
                        ..
                    } => {
                        Some(SystemEvent::Resized)
                    }
                    Event::RedrawEventsCleared => {
                        let delta_time = (Instant::now() - self.last_update).as_secs_f32();
                        println!("Run at {} FPS", 1.0 / delta_time);
                        self.last_update = Instant::now();
                        Some(SystemEvent::FrameUpdate { delta_time })
                    }
                    Event::WindowEvent {
                        event: e @ WindowEvent::KeyboardInput { .. },
                        ..
                    } => {
                        Some(SystemEvent::WindowEvent(e))
                    }
                    _ => None,
                };
                if let Some(e) = system_event {
                    for subsystem in &mut self.subsystems {
                        subsystem.on_event(&e);
                    }    
                }
            })
    }
}
