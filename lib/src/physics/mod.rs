
mod world;
mod collider;

pub use world::World;

//
// Coordinate system:
// y
// ^
// z x >
//
// z points away from the screen
//