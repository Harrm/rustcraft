use std::sync::Arc;

use vulkano::{
    buffer::{cpu_pool::CpuBufferPoolSubbuffer, BufferUsage, CpuBufferPool, TypedBufferAccess},
    command_buffer::{AutoCommandBufferBuilder, PrimaryAutoCommandBuffer},
    descriptor_set::PersistentDescriptorSet,
    device::Device,
    memory::pool::StdMemoryPool,
    pipeline::PipelineBindPoint,
    render_pass::RenderPass,
    sampler::{Filter, MipmapMode, Sampler, SamplerAddressMode},
};

use super::{
    builtin_shaders::vs,
    camera::{Camera, CameraData},
    pipelines::StaticMeshPipeline,
    primitives::Vertex,
    static_mesh::StaticMesh,
};

pub trait MeshRenderer<Mesh> {
    fn render_mesh<'b>(
        &self,
        command_buffer_builder: &'b mut AutoCommandBufferBuilder<PrimaryAutoCommandBuffer>,
        mesh: &Mesh,
        offset: u32, size: u32
    ) -> &'b mut AutoCommandBufferBuilder<PrimaryAutoCommandBuffer>;

    fn update_camera_data(&mut self, camera: CameraData);

    fn recreate_pipeline(
        &mut self,
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        dimensions: &[u32; 2],
    );

    fn get_pipeline(&self) -> &StaticMeshPipeline;
 
}

struct MeshRendererBase<UniformData> {
    camera: CameraData,
    uniform_buffer: CpuBufferPool<UniformData>,
}

impl<UniformData> MeshRendererBase<UniformData> {
    fn get_mvp_matrix(
        &self,
        uniform_data: UniformData,
    ) -> Arc<CpuBufferPoolSubbuffer<UniformData, Arc<StdMemoryPool>>> {
        self.uniform_buffer.next(uniform_data).unwrap()
    }
}

pub struct StaticMeshRenderer {
    base: MeshRendererBase<vs::ty::Data>,
    pipeline: StaticMeshPipeline,
    sampler: Arc<Sampler>,
}

impl StaticMeshRenderer {
    pub fn make(
        device: Arc<Device>,
        camera: CameraData,
        pipeline: StaticMeshPipeline,
    ) -> StaticMeshRenderer {
        let uniform_buffer =
            CpuBufferPool::<vs::ty::Data>::new(device.clone(), BufferUsage::uniform_buffer());

        let sampler = Sampler::new(
            device.clone(),
            Filter::Nearest,
            Filter::Nearest,
            MipmapMode::Nearest,
            SamplerAddressMode::ClampToEdge,
            SamplerAddressMode::ClampToEdge,
            SamplerAddressMode::ClampToEdge,
            0.0,
            1.0,
            0.0,
            0.0,
        )
        .unwrap();

        StaticMeshRenderer {
            base: MeshRendererBase {
                uniform_buffer,
                camera,
            },
            pipeline,
            sampler,
        }
    }
}

impl MeshRenderer<StaticMesh> for StaticMeshRenderer {

    fn get_pipeline(&self) -> &StaticMeshPipeline {
        &self.pipeline
    }

    fn render_mesh<'b>(
        &self,
        command_buffer_builder: &'b mut AutoCommandBufferBuilder<PrimaryAutoCommandBuffer>,
        mesh: &StaticMesh,
        offset: u32, size: u32,
    ) -> &'b mut AutoCommandBufferBuilder<PrimaryAutoCommandBuffer> {
        let layout = self.pipeline.get_vk_layout().descriptor_set_layouts()[0].clone();
  
        let (view, proj) = self.base.camera.get_view_projection_matrix();
        let mvp_subbuffer = self.base.get_mvp_matrix(vs::ty::Data {
            world: mesh.get_transform().into(),
            proj: proj.into(),
            view: view.into(),
        });
        let mvp_set = {
            let mut builder = PersistentDescriptorSet::start(layout.clone());
            builder.add_buffer(mvp_subbuffer).unwrap();
            builder.build().unwrap()
        };
        let tex_layout = self.pipeline.get_vk_layout().descriptor_set_layouts()[1].clone();
        let tex_set = {
            let mut builder = PersistentDescriptorSet::start(tex_layout.clone());
            builder
                .add_sampled_image(mesh.material.get_image().unwrap(), self.sampler.clone())
                .unwrap();
            builder.build().unwrap()
        };
        command_buffer_builder
            .bind_descriptor_sets(
                PipelineBindPoint::Graphics,
                self.pipeline.get_vk_layout(),
                0,
                (mvp_set, tex_set),
            )
            .draw(size, 1, offset, 0)
            .unwrap()
    }

    fn update_camera_data(&mut self, camera: CameraData) {
        self.base.camera = camera;
    }

    fn recreate_pipeline(
        &mut self,
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        dimensions: &[u32; 2],
    ) {
        self.pipeline
            .recreate(device.clone(), render_pass.clone(), &dimensions);
    }
}
