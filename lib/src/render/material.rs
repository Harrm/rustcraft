use std::{
    io::{BufRead, Seek},
    sync::Arc,
};

use image::io::Reader;
use vulkano::{
    device::Queue,
    format::Format,
    image::{
        view::{ImageView, ImageViewCreationError},
        ImmutableImage, MipmapsCount,
    },
};

pub(crate) struct Material {
    image: Arc<ImmutableImage>,
}

impl Material {
    pub(crate) fn create<R: BufRead + Seek>(texture_data_input: R, queue: Arc<Queue>) -> Material {
        let reader = Reader::new(texture_data_input);
        let image = reader
            .with_guessed_format()
            .unwrap()
            .decode()
            .unwrap()
            .to_rgba8();
        let (w, h) = (image.width(), image.height());
        let image_data = image.into_raw().clone();

        // TODO(Harrm): need to pay attention to this future
        let (texture, _tex_future) = {
            ImmutableImage::from_iter(
                image_data.iter().cloned(),
                vulkano::image::ImageDimensions::Dim2d {
                    height: h,
                    width: w,
                    array_layers: 1,
                },
                MipmapsCount::One,
                Format::R8G8B8A8_SRGB,
                queue.clone(),
            )
            .unwrap()
        };

        Material { image: texture }
    }

    pub(crate) fn get_image(
        &self,
    ) -> Result<Arc<ImageView<ImmutableImage>>, ImageViewCreationError> {
        ImageView::new(self.image.clone())
    }
}
