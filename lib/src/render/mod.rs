use std::rc::Rc;

mod builtin_shaders;
mod camera;
mod mesh_renderer;
mod pipelines;
mod primitives;
mod render_subsystem;
mod renderer;
mod scene;
mod static_mesh;
mod material;

pub(crate) use {render_subsystem::RenderSubsystem, scene::Scene};

pub(crate) struct StaticMesh {
    mesh: Rc<static_mesh::StaticMesh>,
}

impl StaticMesh {
    pub(crate) fn create(mesh: Rc<static_mesh::StaticMesh>) -> StaticMesh {
        StaticMesh {
            mesh,
        }
    }

    pub(crate) fn add_offset(&mut self, _offset: cgmath::Vector3<f32>) {
        unimplemented!()
    }

    pub(crate) fn rotate(&mut self, _angle: cgmath::Quaternion<f32>) {
        unimplemented!()
    }
}
