use std::cell::RefCell;
use std::rc::Rc;
use std::sync::Arc;

use cgmath::{Deg, Vector3, Zero};
use vulkano::device::{Device, Queue};
use vulkano::swapchain::{Capabilities, Surface};
use winit::event::ElementState;
use winit::window::Window;

use crate::subsystem::{Subsystem, SystemEvent};

use super::renderer::Renderer;
use super::renderer::RendererData;
use super::Scene;

struct CameraMovement {
    movement: Vector3<f32>,
    y_rotation: f32,
}

impl Default for CameraMovement {
    fn default() -> Self {
        Self {
            movement: Vector3::zero(),
            y_rotation: 0.0,
        }
    }
}

pub struct RenderSubsystem {
    renderer: RendererData,
    scene: Rc<RefCell<Scene>>,

    // camera transform in this frame
    cam_mov: CameraMovement,
}

impl RenderSubsystem {
    pub fn create(
        device: Arc<Device>,
        queue: Arc<Queue>,
        surface: Arc<Surface<Window>>,
        capabilities: Capabilities,
    ) -> RenderSubsystem {
        let scene = Rc::new(RefCell::new(Scene::create(device.clone(), queue.clone())));

        let renderer = super::renderer::RendererData::create(
            capabilities,
            device.clone(),
            queue.clone(),
            surface.clone(),
            scene.clone(),
        );

        RenderSubsystem {
            renderer,
            scene: scene.clone(),
            cam_mov: Default::default(),
        }
    }

    pub fn get_scene(&self) -> Rc<RefCell<Scene>> {
        self.scene.clone()
    }

    fn on_pressed(&mut self, input: &winit::event::KeyboardInput) {
        match input.virtual_keycode {
            Some(winit::event::VirtualKeyCode::W) => self.cam_mov.movement.z = -1.0,
            Some(winit::event::VirtualKeyCode::S) => self.cam_mov.movement.z = 1.0,
            Some(winit::event::VirtualKeyCode::D) => self.cam_mov.movement.x = 1.0,
            Some(winit::event::VirtualKeyCode::A) => self.cam_mov.movement.x = -1.0,
            Some(winit::event::VirtualKeyCode::Q) => self.cam_mov.y_rotation = 90.0,
            Some(winit::event::VirtualKeyCode::E) => self.cam_mov.y_rotation = -90.0,
            Some(..) => {}
            None => {}
        }
    }

    fn on_released(&mut self, input: &winit::event::KeyboardInput) {
        match input.virtual_keycode {
            Some(winit::event::VirtualKeyCode::W) => self.cam_mov.movement.z = 0.0,
            Some(winit::event::VirtualKeyCode::S) => self.cam_mov.movement.z = 0.0,
            Some(winit::event::VirtualKeyCode::D) => self.cam_mov.movement.x = 0.0,
            Some(winit::event::VirtualKeyCode::A) => self.cam_mov.movement.x = 0.0,
            Some(winit::event::VirtualKeyCode::Q) => self.cam_mov.y_rotation = 0.0,
            Some(winit::event::VirtualKeyCode::E) => self.cam_mov.y_rotation = 0.0,
            Some(..) => {}
            None => {}
        }
    }
}

impl Subsystem for RenderSubsystem {
    fn on_event(&mut self, event: &crate::subsystem::SystemEvent) {
        match event {
            SystemEvent::Resized => self.renderer.resize(),
            SystemEvent::FrameUpdate { delta_time } => {
                if self.scene.borrow().needs_rebuild_vertex_buffer() {
                    self.scene.borrow_mut().rebuild_vertex_buffer();
                }
                self.renderer.update();
                self.renderer
                    .move_camera(self.cam_mov.movement * *delta_time);
                self.renderer.rotate_camera(
                    cgmath::Euler::new(
                        Deg(0.0),
                        Deg(self.cam_mov.y_rotation * delta_time),
                        Deg(0.0),
                    )
                    .into(),
                );
            }
            SystemEvent::WindowEvent(winit::event::WindowEvent::KeyboardInput {
                input, ..
            }) => {
                if ElementState::Pressed == input.state {
                    self.on_pressed(input);
                } else {
                    self.on_released(input);
                }
            },
            _ => ()
        }
    }
}
