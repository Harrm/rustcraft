use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::rc::Rc;
use std::sync::Arc;

use cgmath::Matrix4;
use vulkano::buffer::{BufferUsage, CpuAccessibleBuffer, DeviceLocalBuffer};
use vulkano::command_buffer::{AutoCommandBufferBuilder, CommandBufferUsage, PrimaryCommandBuffer};
use vulkano::device::{Device, Queue};
use wavefront_obj::obj::ObjSet;
use wavefront_obj::obj::Object;

use super::material::Material;
use super::primitives::Vertex;
use super::static_mesh::StaticMesh;

pub struct Scene {
    meshes: Vec<Rc<StaticMesh>>,
    device: Arc<Device>,
    queue: Arc<Queue>,

    vertex_buffer: Option<Arc<DeviceLocalBuffer<[Vertex]>>>,
    dirty: bool,
}

impl Scene {
    pub fn create(device: Arc<Device>, queue: Arc<Queue>) -> Scene {
        Scene {
            meshes: vec![],
            device,
            queue,

            dirty: false,
            vertex_buffer: None,
        }
    }

    pub fn download_obj_set(path: &Path) -> ObjSet {
        let mut file = match File::open(&path) {
            Err(why) => panic!("couldn't open {}: {}", path.display(), why),
            Ok(file) => file,
        };
        let mut s = String::new();
        match file.read_to_string(&mut s) {
            Err(why) => panic!("couldn't read {}: {}", path.display(), why),
            Ok(_) => (),
        }

        wavefront_obj::obj::parse(s).unwrap()
    }

    pub(crate) fn download_material(&self, texture: &Path) -> Rc<Material> {
        let reader = std::io::BufReader::new(std::fs::File::open(texture).unwrap());
        Rc::new(Material::create(reader, self.queue.clone()))
    }

    pub(crate) fn add_object(
        &mut self,
        object: &Object,
        transform: Matrix4<f32>,
        material: Rc<Material>,
    ) -> super::StaticMesh {
        self.dirty = true;
        let mesh = Rc::new(StaticMesh::create_from_obj(
            transform.clone(),
            object,
            self.device.clone(),
            self.queue.clone(),
            material,
        ));
        self.meshes.push(mesh.clone());
        super::StaticMesh { mesh: mesh.clone() }
    }

    pub(crate) fn needs_rebuild_vertex_buffer(&self) -> bool {
        self.dirty
    }

    pub(crate) fn rebuild_vertex_buffer(&mut self) {
        let vertices = self
            .meshes
            .iter()
            .flat_map(|m| m.vertices.iter().map(|vert| {
                let v = cgmath::Vector4 { x: vert.position.0, y: vert.position.1, z: vert.position.2, w: 1.0 };
                let v = m.transform * v;
                Vertex { position: (v.x, v.y, v.z), normal: vert.normal, uv: vert.uv }
            }))
            .collect::<Vec<_>>();
        let staging_buffer = CpuAccessibleBuffer::from_iter(
            self.device.clone(),
            BufferUsage::all(),
            false,
            vertices.iter().cloned(),
        )
        .unwrap();

        let vertex_buffer = DeviceLocalBuffer::<[Vertex]>::array(
            self.device.clone(),
            (std::mem::size_of::<Vertex>() * vertices.len()) as u64,
            BufferUsage::all(),
            std::iter::once(self.queue.family()),
        )
        .unwrap();

        let mut buffer = AutoCommandBufferBuilder::primary(
            self.device.clone(),
            self.queue.family(),
            CommandBufferUsage::OneTimeSubmit,
        )
        .unwrap();

        buffer
            .copy_buffer(staging_buffer.clone(), vertex_buffer.clone())
            .unwrap();

        buffer.build().unwrap().execute(self.queue.clone()).unwrap();

        self.vertex_buffer = Some(vertex_buffer.clone());

        self.dirty = false;
    }

    pub(crate) fn get_vertex_buffer(&self) -> Arc<DeviceLocalBuffer<[Vertex]>> {
        assert!(!self.dirty);
        self.vertex_buffer.as_ref().unwrap().clone()
    }

    pub(crate) fn iter(&self) -> std::slice::Iter<Rc<StaticMesh>> {
        self.meshes.iter()
    }
}
