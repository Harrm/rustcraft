

#[derive(Default, Copy, Clone)]
pub struct Vertex {
    pub position: (f32, f32, f32),
    pub normal: (f32, f32, f32),
    pub uv: (f32, f32),
}

vulkano::impl_vertex!(Vertex, position, normal, uv);

#[derive(Default, Debug, Clone)]
pub struct InstanceData {
    pub transform: [[f32; 4]; 4],
}
vulkano::impl_vertex!(InstanceData, transform);
