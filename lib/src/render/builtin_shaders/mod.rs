
pub mod vs {
    vulkano_shaders::shader! {
        ty: "vertex",
        path: "src/render/builtin_shaders/vert.glsl"
    } 
}

pub mod instanced_vs {
    vulkano_shaders::shader! {
        ty: "vertex",
        path: "src/render/builtin_shaders/instanced_vert.glsl"
    }

}

pub mod fs {
    vulkano_shaders::shader! {
        ty: "fragment",
        path: "src/render/builtin_shaders/frag.glsl"
    }
}

pub mod pbr_fs {
    vulkano_shaders::shader! {
        ty: "fragment",
        path: "src/render/builtin_shaders/pbr_frag.glsl"
    }
}
