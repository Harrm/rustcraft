#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

layout(location = 0) out vec3 out_normal;
layout(location = 1) out vec2 out_uv;

layout(set = 0, binding = 0) uniform Data {
    mat4 world;
    mat4 view;
    mat4 proj;
} uniforms;

void main() {
    mat4 worldview = uniforms.view * uniforms.world;
    out_normal = normal;
    out_uv = uv;
    gl_Position = uniforms.proj * worldview * vec4(position, 1.0);
}
