use std::iter;
use std::sync::Arc;
use vulkano::{
    device::Device,
    pipeline::{
        graphics::{vertex_input::VertexDefinition, viewport::Viewport},
        GraphicsPipeline,
    },
    render_pass::RenderPass,
    render_pass::Subpass,
};

mod mesh_pipeline;
pub use mesh_pipeline::{InstancedMeshPipeline, StaticMeshPipeline};

pub trait PipelineBuilder {
    fn make<VDef: VertexDefinition>(
        device: Arc<Device>,
        vertex_definition: VDef,
        vs_entry: vulkano::shader::EntryPoint,
        fs_entry: vulkano::shader::EntryPoint,
        render_pass: Arc<RenderPass>,
        dimensions: &[u32; 2],
    ) -> Arc<GraphicsPipeline>;
}

pub struct TrianglePipeline {}

impl PipelineBuilder for TrianglePipeline {
    fn make<VDef: VertexDefinition>(
        device: Arc<Device>,
        vertex_definition: VDef,
        vs_entry: vulkano::shader::EntryPoint,
        fs_entry: vulkano::shader::EntryPoint,
        render_pass: Arc<RenderPass>,
        dimensions: &[u32; 2],
    ) -> Arc<GraphicsPipeline> {
        GraphicsPipeline::start()
            .vertex_input_state(vertex_definition)
            .vertex_shader(vs_entry, ())
            .triangle_list()
            .viewports_dynamic_scissors_irrelevant(1)
            .viewports(iter::once(Viewport {
                origin: [0.0, 0.0],
                dimensions: [dimensions[0] as f32, dimensions[1] as f32],
                depth_range: 0.0..1.0,
            }))
            .fragment_shader(fs_entry, ())
            .depth_stencil_simple_depth()
            .render_pass(Subpass::from(render_pass, 0).unwrap())
            .build(device)
            .unwrap()
    }
}

pub struct LinePipeline {}

impl PipelineBuilder for LinePipeline {
    fn make<VDef: VertexDefinition>(
        device: Arc<Device>,
        vertex_definition: VDef,
        vs_entry: vulkano::shader::EntryPoint,
        fs_entry: vulkano::shader::EntryPoint,
        render_pass: Arc<RenderPass>,
        dimensions: &[u32; 2],
    ) -> Arc<GraphicsPipeline> {
        GraphicsPipeline::start()
            .vertex_input_state(vertex_definition)
            .vertex_shader(vs_entry, ())
            .line_list()
            .viewports_dynamic_scissors_irrelevant(1)
            .viewports(iter::once(Viewport {
                origin: [0.0, 0.0],
                dimensions: [dimensions[0] as f32, dimensions[1] as f32],
                depth_range: 0.0..1.0,
            }))
            .fragment_shader(fs_entry, ())
            .depth_stencil_simple_depth()
            .render_pass(Subpass::from(render_pass, 0).unwrap())
            .build(device)
            .unwrap()
    }
}
