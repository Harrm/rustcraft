use std::sync::Arc;

use vulkano::device::Device;
use vulkano::pipeline::Pipeline;
use vulkano::pipeline::graphics::vertex_input::BuffersDefinition;
use vulkano::pipeline::layout::PipelineLayout;
use vulkano::pipeline::GraphicsPipeline;
use vulkano::render_pass::RenderPass;

use crate::render::pipelines::PipelineBuilder;
use crate::render::pipelines::TrianglePipeline;
use crate::render::primitives::InstanceData;
use crate::render::primitives::Vertex;

struct PipelineData {
    pipeline: Arc<GraphicsPipeline>,
    vertex_shader: Arc<vulkano::shader::ShaderModule>,
    fragment_shader: Arc<vulkano::shader::ShaderModule>,
}

pub struct StaticMeshPipeline {
    data: PipelineData,
}

impl StaticMeshPipeline {
    pub fn create(
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        vertex_shader: Arc<vulkano::shader::ShaderModule>,
        fragment_shader: Arc<vulkano::shader::ShaderModule>,
        dimensions: &[u32; 2],
    ) -> StaticMeshPipeline {
        let pipeline = TrianglePipeline::make(
            device.clone(),
            BuffersDefinition::new().vertex::<Vertex>(),
            vertex_shader.entry_point("main").unwrap(),
            fragment_shader.entry_point("main").unwrap(),
            render_pass.clone(),
            &dimensions,
        );

        StaticMeshPipeline {
            data: PipelineData {
                pipeline,
                vertex_shader,
                fragment_shader,
            },
        }
    }

    pub fn recreate(
        &mut self,
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        dimensions: &[u32; 2],
    ) {
        self.data.pipeline = TrianglePipeline::make(
            device.clone(),
            BuffersDefinition::new().vertex::<Vertex>(),
            self.data.vertex_shader.entry_point("main").unwrap(),
            self.data.fragment_shader.entry_point("main").unwrap(),
            render_pass.clone(),
            &dimensions,
        );
    }

    pub fn get_vk_pipeline(&self) -> Arc<GraphicsPipeline> {
        self.data.pipeline.clone()
    }

    pub fn get_vk_layout(&self) -> Arc<PipelineLayout> {
        self.data.pipeline.layout().clone()
    }
}

pub struct InstancedMeshPipeline {
    data: PipelineData,
}

impl InstancedMeshPipeline {
    pub fn create(
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        vertex_shader: Arc<vulkano::shader::ShaderModule>,
        fragment_shader: Arc<vulkano::shader::ShaderModule>,
        dimensions: &[u32; 2],
    ) -> InstancedMeshPipeline {
        let pipeline = TrianglePipeline::make(
            device.clone(),
            BuffersDefinition::new()
                .vertex::<Vertex>()
                .instance::<InstanceData>(),
            vertex_shader.entry_point("main").unwrap(),
            fragment_shader.entry_point("main").unwrap(),
            render_pass.clone(),
            &dimensions,
        );

        InstancedMeshPipeline {
            data: PipelineData {
                pipeline,
                vertex_shader,
                fragment_shader,
            },
        }
    }

    pub fn recreate(
        &mut self,
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        dimensions: &[u32; 2],
    ) {
        self.data.pipeline = TrianglePipeline::make(
            device.clone(),
            BuffersDefinition::new()
                .vertex::<Vertex>()
                .instance::<InstanceData>(),
            self.data.vertex_shader.entry_point("main").unwrap(),
            self.data.fragment_shader.entry_point("main").unwrap(),
            render_pass.clone(),
            &dimensions,
        );
    }

    pub fn get_vk_pipeline(&self) -> Arc<GraphicsPipeline> {
        self.data.pipeline.clone()
    }

    pub fn get_vk_layout(&self) -> &Arc<PipelineLayout> {
        self.data.pipeline.layout()
    }
}
