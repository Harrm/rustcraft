use std::cell::RefCell;
use std::path::PathBuf;
use std::rc::Rc;

use cgmath::Matrix4;

use crate::render::{Scene, StaticMesh};

pub struct World {
    pub objects: Vec<Object>,
    pub render_scene: Rc<RefCell<Scene>>,
}

impl World {
    pub fn spawn_object(&mut self, pos: cgmath::Vector3<f32>) -> &mut Object {
        self.objects.push(Object {
            position: pos,
            components: vec![],
        });
        self.objects.last_mut().unwrap()
    }

    pub(crate) fn get_scene(&self) -> impl std::ops::DerefMut<Target = Scene> + '_ {
        return self.render_scene.borrow_mut();
    }
}

pub trait ObjectComponent {
    fn on_init(&mut self);
    fn on_update(&mut self, delta_time: f32);
}

enum StaticMeshComponentState {
    Created {
        mesh: PathBuf,
        texture: PathBuf,
        transform: Matrix4<f32>,
    },
    Initialized {
        mesh: StaticMesh,
    },
}

pub struct StaticMeshComponent {
    mesh: StaticMesh,
}

impl StaticMeshComponent {
    pub fn create(
        mesh: PathBuf,
        texture: PathBuf,
        transform: Matrix4<f32>,
        world: &mut World,
    ) -> StaticMeshComponent {
        let obj_set = Scene::download_obj_set(&mesh);
        let mut scene = world.get_scene();
        let material = scene.download_material(&texture);
        let mesh = scene.add_object(&obj_set.objects[0], transform.to_owned(), material);
        StaticMeshComponent { mesh }
    }
}

impl ObjectComponent for StaticMeshComponent {
    fn on_init(&mut self) {
        
    }

    fn on_update(&mut self, delta_time: f32) {}
}

pub struct Object {
    pub(crate) components: Vec<Box<dyn ObjectComponent>>,
    pub position: cgmath::Vector3<f32>,
}

impl Object {
    pub fn add_component(&mut self, component: Box<dyn ObjectComponent>) {
        self.components.push(component);
        let component = self.components.last_mut().unwrap();
    }

    pub fn on_update(&mut self, delta_time: f32) {}
}
