
use winit::event::WindowEvent as WinitWindowEvent;

pub enum SystemEvent<'a> {
    Resized,
    FrameUpdate { delta_time: f32 },
    WindowEvent(WinitWindowEvent<'a>),
    Shutdown,
}

pub trait Subsystem {
    fn on_event(&mut self, event: &SystemEvent);
}
